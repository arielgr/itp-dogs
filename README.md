# Magento 2 ITP Program

## Automated Setup for Linux

### Prerequisites
1. Install Docker [how to install](https://docs.docker.com/install/)
1. Install Docker Compose [how to install](https://docs.docker.com/compose/install/)

### Instructions
1. Clone repository using `git clone https://<YOUR USERNAME>@bitbucket.org/lazaroRF/itp-magento2.git`
1. Change into the <PROJECT NAME> directory `cd <PROJECT NAME>`
1. Run the following command and follow the on-screen prompts.

        ./installer.sh

1. **Happy Magento Coding**

## Manual Setup

1. Before setting up your local make the proper modification for your project in the following files
 
    - apache2/vhosts.conf.dist (Vhost configurations)
    - docker/Dockerfile.dist (PHP version or dependencies)
    - docker-compose.yml.dist (Subnet and additional images needed)
    - html/auth.json.dist (Add your Magento Public and Private key needed for composer)
    - README.md in case you need to add specific instructions to your teammates
    
1. After all proper modifications are done, please push the changes to your project specific repository.
1. **Happy Magento Coding**

## Built With
* [PHP 7.1](http://php.net/) - Scripting Language
* [Magento 2.3](https://devdocs.magento.com/guides/v2.3/install-gde/system-requirements2.html) - The Web Framework Used
* [Composer](https://getcomposer.org/) - Dependency Management
* [Apache2.4](https://httpd.apache.org/) - HTTP Server
* [MySQL 5.7](https://www.mysql.com/) - Database

## Versioning

We use [BitBucket](https://bitbucket.org) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/lazaroRF/itp-magento2). 

## Authors

**Gamma Partners LLC** - [Gamma Partners site](http://www.gammapartners.com/)

## Collaborator
**Christopher Jaquez** - [chrisjaquezgamma](https://bitbucket.org/chrisjaquezgamma/)

**Minerva Rubalcava** - [arubalcava-gp](https://bitbucket.org/arubalcava-gp/)

**Lázaro Reyes** - [lazaroRF](https://bitbucket.org/lazaroRF/)