FROM ubuntu:16.04
LABEL "AUTHOR"="GAMMA PARTNERS LLC" \
	  "PROJECT"="<PROJECT NAME>"
ADD entrypoint.sh /usr/local/bin/
ADD composer/auth.json /tmp/composer/auth.json
ARG USER_NAME
ARG USER_PASS
ARG PROJECT_CODE
ENV USER_NAME ${USER_NAME:-gamma}
ENV USER_PASS ${USER_PASS:-Gamma035}
ENV PROJECT_CODE ${PROJECT_CODE}
RUN /bin/bash -c "apt-get update; \
    apt-get upgrade -y; \
    apt-get install sudo cron sendmail wget curl vim software-properties-common unzip apache2 -y; \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C; \
    add-apt-repository -y ppa:ondrej/php; \
    apt-add-repository ppa:fish-shell/release-2; \
    apt-get update; \
    apt-get install fish -y; \
    apt-get install php7.1 php7.1-dev php7.1-xml php7.1-ctype php7.1-curl php7.1-soap php7.1-json php7.1-gd php7.1-iconv php7.1-mbstring php7.1-mcrypt php7.1-intl php7.1-zip php7.1-mysql php7.1-bcmath imagemagick php7.1-opcache libapache2-mod-php7.1 -y; \
    apt-get install libapache2-mod-fastcgi php7.1-fpm -y; \
    a2enmod rewrite actions fastcgi alias proxy_fcgi; \
    a2dismod php7.1; \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer; \
    mkdir /tmp/downloads; \
    wget -O /tmp/downloads/xdebug-2.6.1.tgz https://xdebug.org/files/xdebug-2.6.1.tgz; \
    cd /tmp/downloads; \
    tar -xvzf /tmp/downloads/xdebug-2.6.1.tgz; \
    cd /tmp/downloads/xdebug-2.6.1; \
    phpize; \
    ./configure && make; \
    cp /tmp/downloads/xdebug-2.6.1/modules/xdebug.so /usr/lib/php/20160303/; \
    rm -r /tmp/downloads; \
    cp /tmp/composer/auth.json ~/.composer/auth.json; \
    echo \"ServerName 127.0.0.1\" >> /etc/apache2/apache2.conf; \
    rm -r /etc/apache2/sites-enabled/*; \
    ln -s /etc/apache2/sites-available/${PROJECT_CODE}.conf /etc/apache2/sites-enabled/; \
    ln -s /var/www/${PROJECT_CODE}/html/bin/magento /usr/local/bin/magento; \
    sed -i \"s|;date.timezone =|date.timezone = America/Chicago|g\" /etc/php/7.1/fpm/php.ini; \
    sed -i \"s/memory_limit = 128M/memory_limit = 2G/g\" /etc/php/7.1/fpm/php.ini; \
    sed -i \"s/;opache.enable/opcache.enable/g\" /etc/php/7.1/fpm/php.ini; \
    sed -i \"s/;opache.save_comments/opcache.save_comments/g\" /etc/php/7.1/fpm/php.ini; \
    chmod +x /usr/local/bin/entrypoint.sh; \
    echo ${USER_NAME}; \
    echo ${USER_PASS}; \
    useradd ${USER_NAME} --groups sudo,www-data,adm; \
    echo -e \"${USER_PASS}\n${USER_PASS}\" | passwd ${USER_NAME}; \
    mkdir /home/${USER_NAME}; \
    chown -R ${USER_NAME}.${USER_NAME} /home/${USER_NAME}; \
    sed -i \"s|APACHE_RUN_USER=www-data|APACHE_RUN_USER=${USER_NAME}|g\" /etc/apache2/envvars; \
    sed -i \"s|APACHE_RUN_GROUP=www-data|APACHE_RUN_GROUP=${USER_NAME}|g\" /etc/apache2/envvars; \
    sed -i \"s|user = www-data|user = ${USER_NAME}|g\" /etc/php/7.1/fpm/pool.d/www.conf; \
    sed -i \"s|group = www-data|group = ${USER_NAME}|g\" /etc/php/7.1/fpm/pool.d/www.conf; \
    sed -i \"s|listen.owner = www-data|listen.owner = ${USER_NAME}|g\" /etc/php/7.1/fpm/pool.d/www.conf; \
    sed -i \"s|listen.group = www-data|listen.group = ${USER_NAME}|g\" /etc/php/7.1/fpm/pool.d/www.conf; \
    sed -i \"s|;listen.mode|listen.mode|g\" /etc/php/7.1/fpm/pool.d/www.conf; \
    chown -R ${USER_NAME}.${USER_NAME} /var/lib/apache2/fastcgi/;"
CMD ["entrypoint.sh"]
ENTRYPOINT ["entrypoint.sh"]
WORKDIR /var/www/${PROJECT_CODE}/html