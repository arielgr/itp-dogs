<?php


namespace Gamma\Dogs\Api;

use Gamma\Dogs\Api\Data\DogInterface;
interface DogFinderInterface
{
    public function getDog(): DogInterface;
}