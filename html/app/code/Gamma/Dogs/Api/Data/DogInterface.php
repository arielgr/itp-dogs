<?php


namespace Gamma\Dogs\Api\Data;


interface DogInterface
{

    const NAME = 'name';
    const IMAGE = 'image';
    const SUB_BREEDS = 'subbreeds';

    public function getNames(): array;
    public function setNames(array $names): DogInterface;

    public function getImage(): string;
    public function setImage(string $image): DogInterface;

    public function getSub_breeds(): array ;
    public function setSub_breeds(array $sub_breeds): DogInterface;

}