<?php


namespace Gamma\Dogs\Api;


interface ConnectionInterface
{
    public function get(string $resourcePath): array;
}