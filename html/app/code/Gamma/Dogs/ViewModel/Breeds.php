<?php


namespace Gamma\Dogs\ViewModel;

use Gamma\Dogs\Model\DogFinder;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\App\RequestInterface;


class Breeds implements ArgumentInterface
{

    protected $request;

    public function __construct(
        DogFinder $dogFinder,
        RequestInterface $request
    )
    {
        $this->dogFinder = $dogFinder;
        $this->request = $request;
    }

    public function woof(int $volume)
    {
        return $volume > 10 ? 'WOOF' : 'woof';
    }

    public function dog(){
        $dog = $this->dogFinder->getDog();
        return $dog;
    }

}