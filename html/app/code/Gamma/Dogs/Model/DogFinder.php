<?php


namespace Gamma\Dogs\Model;


use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\Data\DogInterface;
use Gamma\Dogs\Api\DogFinderInterface;
use Gamma\Dogs\Api\Data\DogInterfaceFactory;

class DogFinder implements DogFinderInterface
{

    protected $connection;
    protected $dogFactory;


    public function __construct(
        ConnectionInterface $connection,
        DogInterfaceFactory $dogFactory
    )
    {
        $this->connection = $connection;
        $this->dogFactory = $dogFactory;

    }

    public function getDog(): DogInterface
    {
        // TODO: Implement getDog() method.
        $dogData = $this->connection->get('breeds/list/all');
        $dogs = $this->dogFactory->create();

        /*$names = $this->processNames($dogData['message']);*/

        $dogs->setNames($dogData['message']);
            //->setSub_breeds($dogData['message']['bulldog']);


    return $dogs;
    }


}