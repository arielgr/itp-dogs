<?php


namespace Gamma\Dogs\Model\Data;

use Gamma\Dogs\Api\Data\DogInterface;
use Magento\Framework\Api\AbstractSimpleObject;


class Dog extends AbstractSimpleObject implements DogInterface
{

    public function getNames(): array
    {
        // TODO: Implement getName() method.
        return $this->_get(self::NAME);
    }

    public function setNames(array $names): DogInterface
    {
        // TODO: Implement setName() method.
        return $this->setData(self::NAME, $names);
    }

    public function getImage(): string
    {
        // TODO: Implement getImage() method.
        return $this->_get(self::IMAGE);
    }

    public function setImage(string $image): DogInterface
    {
        // TODO: Implement setImage() method.
        return $this->setData(self::IMAGE, $image);

    }

    public function getSub_breeds(): array
    {
        // TODO: Implement getSub_breeds() method.
        return $this->_get(self::SUB_BREEDS);
    }

    public function setSub_breeds(array $sub_breeds): DogInterface
    {
        // TODO: Implement setSub_breeds() method.
        return $this->setData(self::SUB_BREEDS, $sub_breeds);
    }

    /*public function setName(array $names): DogInterface
    {
        // TODO: Implement setName() method.
    }*/
}